const express = require('express');
const router = express.Router();
const Livro = require('../models/livro');

// Base de dados ficticia
//const listaDeLivros = require('./../seed/livros_seed');


router.get('/',(req, res) =>{
//   res.render('livros/index', {livros: listaDeLivros });
  Livro.find({})
    .then((livros) => {
      console.log(livros);
      res.render('livros/index', { livros });
  })
    .catch((error) => {
      console.log(`Ocorreu um erro: ${error}`);
  });
  
});

router.get('/novo', (req, res) => {
  res.render('livros/novo');
});

router.post('/novo', (req, res) => {

  console.log(req.body);
  
  const novoLivro = new Livro({
    isbn: req.body.isbn,
    titulo: req.body.titulo,
    edicao: req.body.edicao
  });
  
  novoLivro.save()
    .then(() =>{ 
       
         req.flash('success', 'O Livro foi cadastrado com sucesso');
         res. redirect('/livros');
     })
    .catch((error) =>{
    console.log(`ocorreu o seguinte erro: ${error}`);
  });
  
});

router.get('/:id/delete', (req,res) =>{
  Livro.findById(req.params.id)
  .then((livro) => res.render('livros/delete.hbs', { livro }))
  .catch((error) => console.log(error));
});

router.delete('/:id', (req, res) => {
  Livro.findByIdAndRemove(req.params.id)
  .then(() => res.redirect ('/livros'));
});


module.exports = router;