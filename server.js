const express = require('express');
const app     = express();
const port    = process.env.PORT || 8080;
const hbs     = require('hbs');
const livroRoutes = require('./routes/livro');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const flash = require('connect-flash');
const session = require('express-session');
const methodOverride = require('method--override');

//Database connection
mongoose.connect('mongodb://nassauweb_user:nassauweb123@ds143767.mlab.com:43767/nassauwebnicky');
mongoose.connection
  .once('open', () => {
    console.log('Tudo tranquilo!');
})
  .on('error', (error) => {
    console.warn(`Deu ruim! ocorreu o seguinte erro: ${error}`);
});

app.use(methodOverride('_method'));

mongoose.Promise = global.Promise;

//Pasta Pública
app.use(express.static(__dirname + '/public'));

//Templete engine - HandLebars
app.set('view engine', 'hbs');

//configuracao das paginas parciais
hbs.registerPartials(__dirname + '/views/partials');

//Configuração do bodyParser
app.use(bodyParser.urlencoded({extend: true }));

app.use(flash());

app.use(session({
  secret:'secret',
  resave:true,
  saveUninitialized: true,
  cookie:{maxAge:6000}
}));

app.use(function(req, res, next){
  res.locals.success  = req.flash('success');
  res.locals.error    = req.flash('error');
  res.locals.info     = req.flash('info');
  
  next();
});

//rota de livros
app.use('/livros', livroRoutes);

app.get('/', (req, res) => {
   res.render('index');
});

// app.listen(port, () => {
//   console.log(`The server is running on port ${port}.`);
// });

app.get('livros', (req, res)=> {
  res.send('Livrosss');
});

app.listen(port, () => {
  console.log(`The server is running on port ${port}.`);
});