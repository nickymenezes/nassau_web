const mongoose = require ('mongoose');
const Schema = mongoose.Schema;

const LivroSchema = new Schema({
  isbn: String,
  titulo: String,
  edicao: Number
});

const Livro = mongoose.model('Livro', LivroSchema);

module.exports = Livro;